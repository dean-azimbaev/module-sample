const { random, all } = require("@dean-azimbaev/dean-module-example");

(() => {
  for (const joke of all) {
    const r = random();

    if (joke === r) {
      console.log(`В точку joke: ${joke}, random: ${r}`);
    } else {
      console.table({
        random: r,
        joke,
      });
    }
  }
})();
