const db = require("./db.json");
const random = require("./random");

module.exports = {
  all: db.jockes,
  random: () => random(db.jockes),
  first: () => (db.jockes[0] ? db.jockes[0] : { nu: null }),
};
