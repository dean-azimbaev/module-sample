const expect = require("chai").expect;
const jockes = require("./index");

describe("Jockes", () => {
  describe("all", () => {
    it("should be an array of strings", () => {
      expect(jockes.all).to.satisfy(isArrayOfStrings);

      function isArrayOfStrings(array) {
        return array.every((item) => typeof item === "string");
      }
    });
  });

  describe("random", () => {
    it("should return a random item from jockes.all", () => {
      const random = jockes.random();

      expect(jockes.all).to.include(random);
    });
  });
});
